FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/test03.sh"]

COPY test03.sh /usr/bin/test03.sh
COPY target/test03.jar /usr/share/test03/test03.jar
